import 'package:flutter/material.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:skeleton_animation/skeleton_animation.dart';

class CustomAllNewsLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    double itemWidth = (size.width - PADDING_SIZE * 3) / 2;

    return GridView.count(
        crossAxisCount: 2,
        crossAxisSpacing: PADDING_SIZE,
        mainAxisSpacing: PADDING_SIZE_SMALL,
        padding: EdgeInsets.symmetric(horizontal: PADDING_SIZE),
        shrinkWrap: true,
        childAspectRatio: 0.6,
        children: List<Widget>.generate(
            6,
            (i) => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Skeleton(
                        borderRadius: BorderRadius.circular(BORDER_RADIUS),
                        style: SkeletonStyle.box,
                        textColor: Colors.grey[300],
                        width: itemWidth,
                        height: itemWidth,
                      ),
                    ),
                    SizedBox(
                      height: PADDING_SIZE_SMALL,
                    ),
                    Skeleton(
                      textColor: Colors.grey[300],
                      height: 16,
                      width: itemWidth - 30,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Skeleton(
                      textColor: Colors.grey[300],
                      height: 16,
                      width: itemWidth - 10,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Skeleton(
                      textColor: Colors.grey[300],
                      height: 16,
                      width: itemWidth,
                    ),
                    SizedBox(
                      height: PADDING_SIZE_SMALL,
                    ),
                    Skeleton(
                      textColor: Colors.grey[300],
                      width: itemWidth - 20,
                      height: 16,
                    ),
                  ],
                )));
  }
}
