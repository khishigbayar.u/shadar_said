import 'package:flutter/material.dart';
import 'package:shadar_said/models/BranchModel.dart';
import 'package:shadar_said/services/BranchService.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:shadar_said/component/CustomBranchHorizontalListItem.dart';
import 'package:provider/provider.dart';

class CustomBranchHorizontalList extends StatelessWidget {
  final Function changeBranch;
  const CustomBranchHorizontalList({Key? key, required this.changeBranch})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final branchService = Provider.of<BranchService>(context);
    List<BranchModel> branchData = branchService.branchData;
    final Size size = MediaQuery.of(context).size;
    final double containerHeight = size.width < 600 ? 80 : 108;

    return Container(
      height: containerHeight,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: branchData.length,
          padding: EdgeInsets.symmetric(horizontal: PADDING_SIZE - 8.0),
          itemBuilder: (context, index) {
            return GestureDetector(
                onTap: () {
                  changeBranch(branchData[index]);
                },
                child: CustomBranchHorizontalListItem(
                    imageUrl: branchData[index].logoUrl,
                    name: branchData[index].shortName));
          },
        ),
      ),
    );
  }

  //   // Loader
  //   return Container(
  //     height: 92,
  //     child: ListView.builder(
  //       scrollDirection: Axis.horizontal,
  //       itemCount: 5,
  //       padding: EdgeInsets.symmetric(horizontal: PADDING_SIZE - 8.0),
  //       itemBuilder: (context, index) {
  //         return CustomBranchHorizontalListItemLoading();
  //       },
  //     ),
  //   );
  // });
  // }
}
