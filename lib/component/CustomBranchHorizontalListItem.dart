import 'package:flutter/material.dart';

class CustomBranchHorizontalListItem extends StatelessWidget {
  final String imageUrl;
  final String name;

  const CustomBranchHorizontalListItem(
      {Key? key, required this.imageUrl, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final Size size = MediaQuery.of(context).size;
    final double imageSize = size.width < 600 ? 40 : 70;
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        children: [
          ClipOval(
            child: Image.network(
              imageUrl,
              height: imageSize,
              width: imageSize,
              fit: BoxFit.cover,
            ),
          ),
          Text(name, style: themeData.textTheme.bodyText2)
        ],
      ),
    );
  }
}
