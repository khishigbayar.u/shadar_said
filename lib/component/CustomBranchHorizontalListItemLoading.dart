import 'package:flutter/material.dart';
import 'package:skeleton_animation/skeleton_animation.dart';

class CustomBranchHorizontalListItemLoading extends StatelessWidget {
  const CustomBranchHorizontalListItemLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double imageSize = size.width < 600 ? 40 : 70;
    final double textSize = size.width < 600 ? 14 : 18;
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        children: [
          ClipOval(
            child: Skeleton(
              textColor: Colors.grey[300],
              width: imageSize,
              height: imageSize,
            ),
          ),
          Skeleton(
            textColor: Colors.grey[300],
            width: 50,
            height: textSize,
          ),
        ],
      ),
    );
  }
}
