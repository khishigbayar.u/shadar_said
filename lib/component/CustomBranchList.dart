import 'package:flutter/material.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:shadar_said/models/BranchModel.dart';
import 'package:shadar_said/screen/BranchScreen.dart';
import 'package:shadar_said/services/BranchService.dart';
import 'package:provider/provider.dart';
import 'package:shadar_said/screen/ShadarListScreen.dart';

class CustomBranchList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    final branchService = Provider.of<BranchService>(context);

    List<BranchModel> staticData = [
      BranchModel(
          id: "1",
          logoUrl: "1",
          shortName: "1",
          description: "1",
          address: "1",
          name: "Мэдээлэл"),
      BranchModel(
          id: "2",
          logoUrl: "1",
          shortName: "1",
          description: "1",
          address: "1",
          name: "Шадар сайд"),
      BranchModel(
          id: "3",
          logoUrl: "1",
          shortName: "1",
          description: "1",
          address: "1",
          name: "Агентлагууд")
    ];
    if (branchService.branchData.isNotEmpty) {
      staticData..addAll(branchService.branchData);
    }

    return ListView.builder(
        itemCount: staticData.length,
        padding: EdgeInsets.symmetric(horizontal: PADDING_SIZE - 8.0),
        itemBuilder: (context, index) {
          if (index == 0 || index == 2) {
            return ListTile(
              title: Text(
                staticData[index].name,
                style: themeData.textTheme.headline3!.merge(
                    TextStyle(color: themeData.textTheme.bodyText2!.color)),
              ),
            );
          } else if (index == 1) {
            return ListTile(
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (context, animation, secondaryAnimation) =>
                            ShadarListScreen(),
                        transitionDuration: Duration(milliseconds: 300),
                        transitionsBuilder:
                            (context, animation, secondaryAnimation, child) {
                          var begin = Offset(1, 0.0);
                          var end = Offset(0, 0);
                          var curve = Curves.ease;

                          var tween = Tween(begin: begin, end: end)
                              .chain(CurveTween(curve: curve));

                          return SlideTransition(
                            position: animation.drive(tween),
                            child: child,
                          );
                        },
                      ));
                },
                title: Padding(
                  padding: const EdgeInsets.only(left: PADDING_SIZE_SMALL),
                  child: Text(
                    "Шадар сайд",
                    style: themeData.textTheme.headline3,
                  ),
                ));
          } else {
            return ListTile(
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (context, animation, secondaryAnimation) =>
                            BranchScreen(branchData: staticData[index]),
                        transitionDuration: Duration(milliseconds: 300),
                        transitionsBuilder:
                            (context, animation, secondaryAnimation, child) {
                          var begin = Offset(1, 0.0);
                          var end = Offset(0, 0);
                          var curve = Curves.ease;

                          var tween = Tween(begin: begin, end: end)
                              .chain(CurveTween(curve: curve));

                          return SlideTransition(
                            position: animation.drive(tween),
                            child: child,
                          );
                        },
                      ));
                },
                title: Padding(
                  padding: const EdgeInsets.only(left: PADDING_SIZE_SMALL),
                  child: Row(
                    children: [
                      ClipOval(
                        child: Image.network(
                          staticData[index].logoUrl,
                          height: 36,
                          width: 36,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(width: PADDING_SIZE_SMALL),
                      Expanded(
                        child: Text(
                          staticData[index].name,
                          style: themeData.textTheme.bodyText1,
                          maxLines: 3,
                        ),
                      ),
                    ],
                  ),
                ));
          }
        });
  }

  // // Loader
  // return ListView.builder(
  //     itemCount: 12,
  //     padding: EdgeInsets.symmetric(horizontal: PADDING_SIZE - 8.0),
  //     itemBuilder: (context, index) {
  //       if (index == 0) {
  //         return ListTile(
  //           title: Text(
  //             "Мэдээлэл",
  //             style: themeData.textTheme.headline3.merge(TextStyle(
  //                 color: themeData.textTheme.bodyText2.color)),
  //           ),
  //         );
  //       } else if (index == 1) {
  //         return ListTile(
  //             onTap: () {
  //               Navigator.of(context).pop();
  //               Navigator.push(
  //                   context,
  //                   PageRouteBuilder(
  //                     pageBuilder:
  //                         (context, animation, secondaryAnimation) =>
  //                             ShadarScreen(),
  //                     transitionDuration: Duration(milliseconds: 300),
  //                     transitionsBuilder: (context, animation,
  //                         secondaryAnimation, child) {
  //                       var begin = Offset(1, 0.0);
  //                       var end = Offset(0, 0);
  //                       var curve = Curves.ease;

  //                       var tween = Tween(begin: begin, end: end)
  //                           .chain(CurveTween(curve: curve));

  //                       return SlideTransition(
  //                         position: animation.drive(tween),
  //                         child: child,
  //                       );
  //                     },
  //                   ));
  //             },
  //             title: Padding(
  //               padding:
  //                   const EdgeInsets.only(left: PADDING_SIZE_SMALL),
  //               child: Text(
  //                 "Шадар сайд",
  //                 style: themeData.textTheme.headline3,
  //               ),
  //             ));
  //       } else if (index == 2) {
  //         return ListTile(
  //           title: Text(
  //             "Агентлагууд",
  //             style: themeData.textTheme.headline3.merge(TextStyle(
  //                 color: themeData.textTheme.bodyText2.color)),
  //           ),
  //         );
  //       } else {
  //         return Padding(
  //           padding: const EdgeInsets.symmetric(vertical: 8),
  //           child: Skeleton(
  //               textColor: Colors.grey[300], width: 200, height: 48),
  //         );
  //       }
  //     });
  // }
}
