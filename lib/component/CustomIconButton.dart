import 'package:flutter/material.dart';
import 'package:shadar_said/utils/constants.dart';

class CustomIconButton extends StatelessWidget {
  final Widget child;
  final EdgeInsets padding;
  final double width, height;
  final String type;
  final void Function() onTap;

  const CustomIconButton(
      {Key? key,
      required this.padding,
      required this.width,
      required this.height,
      required this.onTap,
      required this.type,
      required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (type == "raised") {
      return InkWell(
        highlightColor: COLOR_BLUE.withAlpha(80),
        splashColor: COLOR_BLUE.withAlpha(80),
        borderRadius: BorderRadius.circular(20),
        child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(height),
              // border: Border.all(color: COLOR_GREY, width: 2)
            ),
            padding: padding ?? const EdgeInsets.all(8.0),
            child: Center(child: child)),
        onTap: onTap,
      );
    }
    return InkWell(
      highlightColor: COLOR_BLUE.withAlpha(80),
      splashColor: COLOR_BLUE.withAlpha(80),
      borderRadius: BorderRadius.circular(20),
      child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(height),
            // border: Border.all(color: COLOR_GREY, width: 2)
          ),
          padding: padding ?? const EdgeInsets.all(8.0),
          child: Center(child: child)),
      onTap: onTap,
    );
  }
}
