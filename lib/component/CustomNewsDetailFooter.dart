import 'package:flutter/material.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:shadar_said/utils/utilFunctions.dart';

class CustomNewsDetailFooter extends StatelessWidget {
  final String category;
  final String? createdDate;

  const CustomNewsDetailFooter(
      {Key? key, required this.category, this.createdDate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    return Wrap(
      direction: Axis.horizontal,

      children: [
        Text(category,
            style: themeData.textTheme.subtitle1!
                .merge(TextStyle(color: COLOR_BLUE))),
        Container(
          width: 20,
          child: Padding(
            padding: const EdgeInsets.only(top: 4, right: 8, left: 8),
            child: Center(
              child: Icon(
                Icons.brightness_1,
                color: COLOR_GREY,
                size: 6,
              ),
            ),
          ),
        ),
        Text(
          calculateTime(createdDate!),
          style: themeData.textTheme.subtitle2,
        ),
      ],
    );
  }
}
