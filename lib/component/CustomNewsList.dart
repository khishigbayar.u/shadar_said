import 'package:flutter/material.dart';
import 'package:shadar_said/models/NewsModel.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:shadar_said/component/CustomNewsDetailFooter.dart';
import 'package:shadar_said/component/CustomNewsLoading.dart';
import 'package:shadar_said/screen/DetailScreen.dart';

class CustomNewsList extends StatelessWidget {
  final List<NewsModel> newsData;
  const CustomNewsList({required Key key, required this.newsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return newsData.isNotEmpty
        ? Column(
            children: renderNewsWidget(newsData, context),
          )
        : Column(children: renderloading());
  }

  renderloading() {
    List<Widget> newsWidgets = [];
    for (int i = 0; i < 10; i++) {
      newsWidgets.add(CustomNewsLoading());
    }
    return newsWidgets;
  }

  renderNewsWidget(List<NewsModel> newsData, BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final Size size = MediaQuery.of(context).size;
    List<Widget> newsWidgets = [];
    final double _imageSize = (size.width < 600 ? 100 : 150);
    final double _iconSize = 30;
    final double branchTitleWidth = size.width -
        _imageSize -
        (PADDING_SIZE * 2) -
        (PADDING_SIZE_SMALL * 2) -
        _iconSize;
    for (int i = 0; i < newsData.length; i++) {
      newsWidgets.add(Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (context, animation, secondaryAnimation) =>
                      DetailScreen(selectedNews: newsData[i]),
                  transitionDuration: Duration(milliseconds: 300),
                  transitionsBuilder:
                      (context, animation, secondaryAnimation, child) {
                    var begin = Offset(0.8, 0.0);
                    var end = Offset(0, 0);
                    var curve = Curves.ease;

                    var tween = Tween(begin: begin, end: end)
                        .chain(CurveTween(curve: curve));

                    return SlideTransition(
                      position: animation.drive(tween),
                      child: child,
                    );
                  },
                ));
          },
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: PADDING_SIZE_SMALL),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    newsData[i].imageUrl,
                    height: _imageSize,
                    width: _imageSize,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ClipOval(
                          child: Image.network(
                            newsData[i].branch.logoUrl,
                            height: _iconSize,
                            width: _iconSize,
                            fit: BoxFit.cover,
                          ),
                        ),
                        SizedBox(
                          width: PADDING_SIZE_SMALL,
                        ),
                        SizedBox(
                            width: branchTitleWidth,
                            child: Text(newsData[i].branch.name,
                                textAlign: TextAlign.justify,
                                style: themeData.textTheme.bodyText2))
                      ],
                    ),
                    SizedBox(
                      height: PADDING_SIZE_SMALL,
                    ),
                    Container(
                      child: Text(newsData[i].description,
                          style: themeData.textTheme.bodyText1,
                          textAlign: TextAlign.justify,
                          maxLines: 4),
                    ),
                    SizedBox(
                      height: PADDING_SIZE_SMALL,
                    ),
                    CustomNewsDetailFooter(
                        category: newsData[i].category.name,
                        createdDate: newsData[i].createdDate)
                  ],
                ),
              )
            ],
          ),
        ),
      ));
    }

    return newsWidgets;
  }
}
