import 'package:flutter/material.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:skeleton_animation/skeleton_animation.dart';

class CustomNewsLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double _imageSize = (size.width < 600 ? 100 : 150);
    return Column(
        children: List<Widget>.generate(
      2,
      (i) => Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: PADDING_SIZE_SMALL),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Skeleton(
                  borderRadius: BorderRadius.circular(BORDER_RADIUS),
                  style: SkeletonStyle.box,
                  textColor: Colors.grey[300],
                  width: _imageSize,
                  height: _imageSize,
                ),
              ),
            ),
            Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Skeleton(
                    textColor: Colors.grey[300],
                    width: 50,
                    height: 18,
                  ),
                  SizedBox(
                    height: PADDING_SIZE_SMALL,
                  ),
                  Container(
                      child: Column(
                    children: [
                      Skeleton(
                        textColor: Colors.grey[300],
                        height: 8,
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Skeleton(
                        textColor: Colors.grey[300],
                        height: 8,
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Skeleton(
                        textColor: Colors.grey[300],
                        height: 8,
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Skeleton(
                        textColor: Colors.grey[300],
                        height: 8,
                      ),
                    ],
                  )),
                  SizedBox(
                    height: PADDING_SIZE_SMALL,
                  ),
                  Skeleton(
                    textColor: Colors.grey[300],
                    width: 150,
                    height: 16,
                  ),
                ]))
          ],
        ),
      ),
    ));
  }
}
