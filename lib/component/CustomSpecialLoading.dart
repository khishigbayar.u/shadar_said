import 'package:flutter/material.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:skeleton_animation/skeleton_animation.dart';

class CustomSpecialLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Skeleton(
        borderRadius: BorderRadius.circular(BORDER_RADIUS),
        style: SkeletonStyle.box,
        textColor: Colors.grey[300],
        width: size.width - PADDING_SIZE * 2,
        height: 280,
      ),
      SizedBox(
        height: PADDING_SIZE_SMALL,
      ),
      Skeleton(
        textColor: Colors.grey[300],
        width: 50,
        height: 18,
      ),
      SizedBox(
        height: PADDING_SIZE_SMALL,
      ),
      Skeleton(
        textColor: Colors.grey[300],
        style: SkeletonStyle.text,
        width: size.width - PADDING_SIZE * 2,
        height: 22,
      ),
      SizedBox(
        height: 4,
      ),
      Skeleton(
        textColor: Colors.grey[300],
        style: SkeletonStyle.text,
        width: size.width / 2,
        height: 22,
      ),
      SizedBox(
        height: PADDING_SIZE_SMALL,
      ),
      Skeleton(
        textColor: Colors.grey[300],
        width: 150,
        height: 16,
      ),
      SizedBox(
        height: 8,
      ),
    ]));
  }
}
