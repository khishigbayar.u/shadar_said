import 'package:flutter/material.dart';
import 'package:shadar_said/models/NewsModel.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:shadar_said/component/CustomNewsDetailFooter.dart';
import 'package:shadar_said/screen/DetailScreen.dart';

class CustomSpecialNews extends StatelessWidget {
  final NewsModel? itemData;

  const CustomSpecialNews({required Key key, this.itemData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final Size size = MediaQuery.of(context).size;
    return Container(
        child: GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  DetailScreen(selectedNews: itemData),
              transitionDuration: Duration(milliseconds: 300),
              transitionsBuilder:
                  (context, animation, secondaryAnimation, child) {
                var begin = Offset(0.8, 0.0);
                var end = Offset(0, 0);
                var curve = Curves.ease;

                var tween = Tween(begin: begin, end: end)
                    .chain(CurveTween(curve: curve));

                return SlideTransition(
                  position: animation.drive(tween),
                  child: child,
                );
              },
            ));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(BORDER_RADIUS),
            child: Image.network(
              itemData!.imageUrl,
              width: size.width - PADDING_SIZE * 2,
              height: 280,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: PADDING_SIZE_SMALL,
          ),
          Text(
            itemData!.branch.name,
            style: themeData.textTheme.bodyText2,
          ),
          SizedBox(
            height: PADDING_SIZE_SMALL,
          ),
          Text(
            itemData!.title,
            style: themeData.textTheme.headline2,
          ),
          SizedBox(
            height: PADDING_SIZE_SMALL,
          ),
          CustomNewsDetailFooter(
            category: itemData!.category.name,
            createdDate: itemData!.createdDate)
        ],
      ),
    ));
  }
}
