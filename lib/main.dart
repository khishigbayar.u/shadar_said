import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:shadar_said/screen/SplashScreen.dart';
import 'package:shadar_said/services/BranchService.dart';
import 'package:shadar_said/services/ShadarSaidService.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:provider/provider.dart';

void main() async {
  final branchService = BranchService();
  final shadarSaidService = ShadarSaidService();

  runApp(MultiProvider(providers: [
    Provider(create: (context) => branchService),
    Provider(create: (context) => shadarSaidService),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    double screenWidth = window.physicalSize.width;
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          // primarySwatch: COLOR_BLUE,
          primaryColor: COLOR_WHITE,
          accentColor: COLOR_BLUE,
          highlightColor: COLOR_BLUE.withAlpha(40),
          textTheme: screenWidth < 500 ? TEXT_THEME_SMALL : TEXT_THEME_DEFAULT,
          fontFamily: 'Montserrat'),
      home: SplashScreen(),
      // AnimatedSplashScreen(
      //     duration: 3000,
      //     splash: Icons.home,
      //     nextScreen: HomeScreen(),
      //     splashTransition: SplashTransition.slideTransition,
      //     backgroundColor: Color(0xFF1161ef)),
      debugShowCheckedModeBanner: false,
    );
  }
}
