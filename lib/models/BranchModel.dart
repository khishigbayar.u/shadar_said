import 'package:shadar_said/utils/utilFunctions.dart';

class BranchModel {
  BranchModel(
      { required this.id,
        required this.name,
        required this.shortName,
        required this.logoUrl,
        required this.description,
        required this.address,
      this.phoneNumber,
      this.email,
      this.coverImageUrl,
      this.createdDate,
      this.createdUserId,
      this.updatedDate,
      this.updatedUserId});

  final String id;
  final String name;
  final String shortName;
  final String logoUrl;
  final String address;
  final String? phoneNumber;
  final String? email;
  final String? coverImageUrl;
  final String description;
  final String? createdDate;
  final String? createdUserId;
  final String? updatedDate;
  final String? updatedUserId;

  factory BranchModel.fromJson(Map<String, dynamic> json) {
    return BranchModel(
        id: json["_id"] as String,
        name: json["name"] as String,
        shortName: json["shortName"] as String,
        logoUrl: json["logoUrl"] as String,
        description: json['description'] as String,
        address: getNullableString(json['address']),
        phoneNumber: getNullableString(json['phoneNumber']),
        email: getNullableString(json['email']),
        coverImageUrl: getNullableString(json['coverImageUrl']),
        createdDate: getNullableString(json['createdDate']),
        createdUserId: getNullableString(json['createdUserId']),
        updatedDate: getNullableString(json['updatedDate']),
        updatedUserId: getNullableString(json['updatedUserId']));
  }

  Map<String, dynamic> toJson() {
    return {
      "_id": id,
      "name": name,
      "shortName": shortName,
      "logoUrl": logoUrl,
      "address": address,
      "phoneNumber": phoneNumber,
      "email": email,
      "coverImageUrl": coverImageUrl,
      "description": description,
      "createdDate": createdDate,
      "createdUserId": createdUserId,
      "updatedDate": updatedDate,
      "updatedUserId": updatedUserId
    };
  }
}
