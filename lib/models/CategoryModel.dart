import 'package:shadar_said/utils/utilFunctions.dart';

class CategoryModel {
  CategoryModel(
      {
        required this.id,
        required this.name,
      this.description,
      this.createdDate,
      this.createdUserId,
      this.updatedDate,
      this.updatedUserId});

  final String id;
  final String name;
  final String? description;
  final String? createdDate;
  final String? createdUserId;
  final String? updatedDate;
  final String? updatedUserId;

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel(
        id: json["_id"] as String,
        name: json["name"] as String,
        description: getNullableString(json['description']),
        createdDate: getNullableString(json['createdDate']),
        createdUserId: getNullableString(json['createdUserId']),
        updatedDate: getNullableString(json['updatedDate']),
        updatedUserId: getNullableString(json['updatedUserId']));
  }

  Map<String, dynamic> toJson() {
    return {
      "_id": id,
      "name": name,
      "description": description,
      "createdDate": createdDate,
      "createdUserId": createdUserId,
      "updatedDate": updatedDate,
      "updatedUserId": updatedUserId
    };
  }
}
