import 'package:shadar_said/models/BranchModel.dart';
import 'package:shadar_said/models/CategoryModel.dart';
import 'package:shadar_said/utils/utilFunctions.dart';

class NewsModel {
  NewsModel(
      {
        required this.id,
        required this.title,
        required this.imageUrl,
        required this.category,
        required this.branch,
        required this.description,
        required this.createdDate,
        required this.createdUserId,
        required this.updatedDate,
        required this.updatedUserId});

  final String id;
  final String title;
  final String imageUrl;
  final CategoryModel category;
  final String description;
  final BranchModel branch;
  final String createdDate;
  final String createdUserId;
  final String updatedDate;
  final String updatedUserId;

  factory NewsModel.fromJson(Map<String, dynamic> json) {
    return NewsModel(
        id: json["_id"] as String,
        title: json['title'] as String,
        imageUrl: json['imageUrl'] as String,
        branch: BranchModel.fromJson(json['branch']),
        category: CategoryModel.fromJson(json['category']),
        createdDate: getNullableString(json['createdDate']),
        description: getNullableString(json['description']),
        createdUserId: getNullableString(json['createdUserId']),
        updatedDate: getNullableString(json['updatedDate']),
        updatedUserId: getNullableString(json['updatedUserId']));
  }

  Map<String, dynamic> toJson() {
    return {
      "title": title,
      "imageUrl": imageUrl,
      "branch": branch.toJson(),
      "category": category.toJson(),
      "description": description,
      "createdDate": createdDate,
      "createdUserId": createdUserId,
      "updatedDate": updatedDate,
      "updatedUserId": updatedUserId
    };
  }
}
