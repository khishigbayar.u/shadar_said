class ResponseModel {
  int code;
  String data;

  ResponseModel({required this.code, required this.data});

  factory ResponseModel.fromJson(Map<String, dynamic> json) {
    return ResponseModel(
      code: json['code'] as int,
      data: json['data'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "code": code,
      "data": data,
    };
  }
}
