class SaidAwardModel {
  String awardName;
  String year;

  SaidAwardModel({required this.awardName, required this.year});

  factory SaidAwardModel.fromJson(Map<String, dynamic> json) {
    return SaidAwardModel(
      awardName: json['awardName'] as String,
      year: json['year'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "awardName": awardName,
      "year": year,
    };
  }
}
