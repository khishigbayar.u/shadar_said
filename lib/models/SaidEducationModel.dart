class SaidEducationModel {
  String startYear;
  String endYear;
  String schoolName;
  String degree;
  String location;

  SaidEducationModel(
      {required this.startYear,
      required this.endYear,
      required this.schoolName,
      required this.location,
      required this.degree});

  factory SaidEducationModel.fromJson(Map<String, dynamic> json) {
    return SaidEducationModel(
      startYear: json['startYear'] as String,
      endYear: json['endYear'] as String,
      schoolName: json['schoolName'] as String,
      degree: json['degree'] as String,
      location: json['location'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "startYear": startYear,
      "endYear": endYear,
      "schoolName": schoolName,
      "degree": degree,
      "location": location,
    };
  }
}
