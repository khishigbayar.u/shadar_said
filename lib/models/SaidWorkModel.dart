class SaidWorkModel {
  String startYear;
  String endYear;
  String officeName;
  String position;

  SaidWorkModel(
      {required this.startYear,
      required this.endYear,
      required this.officeName,
      required this.position});

  factory SaidWorkModel.fromJson(Map<String, dynamic> json) {
    return SaidWorkModel(
      startYear: json['startYear'] as String,
      endYear: json['endYear'] as String,
      officeName: json['officeName'] as String,
      position: json['position'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "startYear": startYear,
      "endYear": endYear,
      "officeName": officeName,
      "position": position,
    };
  }
}
