import 'package:shadar_said/models/SaidAwardModel.dart';
import 'package:shadar_said/models/SaidEducationModel.dart';
import 'package:shadar_said/models/SaidWorkModel.dart';

class ShadarSaid {
  String imageUrl;
  String fullName;
  String description;
  String year;
  String position;
  List<SaidWorkModel> works = [];
  List<SaidEducationModel> educations = [];
  List<SaidAwardModel>? awards = [];

  ShadarSaid(
      {required this.imageUrl,
      required this.fullName,
      required this.description,
      required this.year,
      required this.position,
      required this.works,
      required this.educations,
      this.awards});

  factory ShadarSaid.fromJson(Map<String, dynamic> json) {
    List<SaidWorkModel> works = [];
    for (int i = 0; i < json['works'].length; i++) {
      works.add(new SaidWorkModel.fromJson(json['works'][i]));
    }
    List<SaidEducationModel> educations = [];
    for (int i = 0; i < json['educations'].length; i++) {
      educations.add(new SaidEducationModel.fromJson(json['educations'][i]));
    }
    List<SaidAwardModel> awards = [];
    for (int i = 0; i < json['awards'].length; i++) {
      awards.add(new SaidAwardModel.fromJson(json['awards'][i]));
    }
    return ShadarSaid(
      imageUrl: json['imageUrl'] as String,
      fullName: json['fullName'] as String,
      description: json['description'] as String,
      year: json['year'] as String,
      position: json['position'] as String,
      works: works,
      educations: educations,
      awards: awards,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "imageUrl": imageUrl,
      "fullName": fullName,
      "description": description,
      "year": year,
      "position": position,
    };
  }
}
