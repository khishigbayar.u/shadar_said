import 'package:flutter/material.dart';
import 'package:shadar_said/models/BranchModel.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:shadar_said/utils/utilFunctions.dart';

class BranchScreen extends StatelessWidget {
  final BranchModel branchData;

  const BranchScreen({Key? key, required this.branchData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final ThemeData themeData = Theme.of(context);
    final sidePadding = EdgeInsets.symmetric(horizontal: PADDING_SIZE);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          toolbarHeight: 80,
          title: Text("Агентлаг", style: themeData.textTheme.headline3),
        ),
        body: Container(
          height: size.height,
          width: size.width,
          child: ListView(children: [
            Padding(
                padding: sidePadding,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: PADDING_SIZE_SMALL,
                      ),
                      Text(
                        branchData.name,
                        style: themeData.textTheme.headline3,
                        maxLines: 3,
                      ),
                      SizedBox(
                        height: PADDING_SIZE_SMALL,
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(BORDER_RADIUS),
                        child: Image.network(
                          getNullableString(branchData.coverImageUrl),
                          width: size.width - PADDING_SIZE * 2,
                          height: 280,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        height: PADDING_SIZE_SMALL,
                      ),
                      Text(
                        checkIsNullOrEmptyString(branchData.description)
                            ? ""
                            : branchData.description,
                        textAlign: TextAlign.justify,
                        style: themeData.textTheme.bodyText1,
                      ),
                      SizedBox(
                        height: PADDING_SIZE_SMALL,
                      ),
                      Text(
                        "Хаяг, байршил",
                        textAlign: TextAlign.justify,
                        style: themeData.textTheme.headline4,
                      ),
                      Text(
                        checkIsNullOrEmptyString(branchData.address)
                            ? ""
                            : branchData.address,
                        textAlign: TextAlign.justify,
                        style: themeData.textTheme.bodyText1,
                      ),
                      SizedBox(
                        height: PADDING_SIZE_SMALL,
                      ),
                      Text(
                        "Холбоо барих",
                        textAlign: TextAlign.justify,
                        style: themeData.textTheme.headline4,
                      ),
                      Text(
                        "Утасны дугаар: ${getNullableString(branchData.phoneNumber)}",
                        textAlign: TextAlign.justify,
                        style: themeData.textTheme.bodyText1,
                      ),
                      Text(
                        "Цахим шуудан: ${getNullableString(branchData.email)}",
                        textAlign: TextAlign.justify,
                        style: themeData.textTheme.bodyText1,
                      ),
                      SizedBox(
                        height: PADDING_SIZE_SMALL,
                      ),
                    ]))
          ]),
        ));
  }
}
