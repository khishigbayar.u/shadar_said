import 'package:flutter/material.dart';
import 'package:shadar_said/component/CustomNewsDetailFooter.dart';
import 'package:shadar_said/models/NewsModel.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:shadar_said/utils/utilFunctions.dart';

class DetailScreen extends StatefulWidget {
  final NewsModel? selectedNews;

  const DetailScreen({Key? key, this.selectedNews}) : super(key: key);

  @override
  _DetailScreenState createState() =>
      _DetailScreenState(selectedNews: this.selectedNews);
}

class _DetailScreenState extends State<DetailScreen> {
  final NewsModel? selectedNews;

  _DetailScreenState({this.selectedNews});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final ThemeData themeData = Theme.of(context);
    final sidePadding = EdgeInsets.symmetric(horizontal: PADDING_SIZE);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        toolbarHeight: 80,
        title: Text("Дэлгэрэнгүй", style: themeData.textTheme.headline3),
      ),
      body: Container(
          width: size.width,
          height: size.height,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(
                child: ListView(scrollDirection: Axis.vertical, children: [
              SizedBox(
                height: PADDING_SIZE_SMALL,
              ),
              Padding(
                  padding: sidePadding,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          getNullableString(selectedNews!.title),
                          style: themeData.textTheme.headline2,
                        ),
                        SizedBox(
                          height: PADDING_SIZE_SMALL,
                        ),
                        CustomNewsDetailFooter(
                            category: selectedNews!.category.name,
                            createdDate: selectedNews!.createdDate),
                        SizedBox(
                          height: PADDING_SIZE_SMALL,
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(BORDER_RADIUS),
                          child: Image.network(
                            getNullableString(selectedNews!.imageUrl),
                            width: size.width - PADDING_SIZE * 2,
                            height: 280,
                            fit: BoxFit.cover,
                          ),
                        ),
                        SizedBox(
                          height: PADDING_SIZE_SMALL,
                        ),
                        Text(
                            getNullableString(selectedNews!.description),
                          textAlign: TextAlign.justify,
                          style: themeData.textTheme.bodyText1,
                        ),
                        SizedBox(
                          height: PADDING_SIZE,
                        ),
                      ]))
            ]))
          ])),
    );
  }
}
