import 'package:flutter/material.dart';
import 'package:shadar_said/component/CustomBranchList.dart';
import 'package:shadar_said/component/CustomSpecialNews.dart';
import 'package:shadar_said/component/CustomNewsList.dart';
import 'package:shadar_said/component/CustomSpecialLoading.dart';
import 'package:shadar_said/models/NewsModel.dart';
import 'package:shadar_said/screen/NewsScreen.dart';
import 'package:shadar_said/services/BranchService.dart';
import 'package:shadar_said/services/NewsService.dart';
import 'package:shadar_said/services/ShadarSaidService.dart';
import 'package:shadar_said/utils/constants.dart';
import 'dart:async';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool hasInternet = true;
  late StreamSubscription _connectivitySubscription;

  final ScrollController _scrollController = ScrollController();
  int page = 0;
  int size = 10;
  bool loading = false;
  bool stopLoader = false;
  List<NewsModel> allNews = [];

  @override
  void initState() {
    super.initState();

    _connectivitySubscription = InternetConnectionChecker()
        .onStatusChange
        .listen((InternetConnectionStatus event) {
      if (event == InternetConnectionStatus.connected) {
        loadNewsData();
        setState(() {
          hasInternet = true;
        });
      } else {
        setState(() {
          page = 0;
          hasInternet = false;
        });
      }
    });

    _scrollController.addListener(() {
      var _loading = loading;
      if (_scrollController.position.pixels >=
              _scrollController.position.maxScrollExtent &&
          hasInternet &&
          !stopLoader &&
          !_loading) {
        loadNewsData();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _connectivitySubscription.cancel();
    _scrollController.dispose();
  }

  renderLastNews() {
    return FutureBuilder<NewsModel>(
      future: getLatestNews(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          NewsModel? news = snapshot.data;
          return CustomSpecialNews(key: Key("specialNews"), itemData: news);
        }
        // Loader
        return CustomSpecialLoading();
      },
    );
  }

  void loadNewsData() {
    setState(() {
      loading = true;
    });
    page = page + 1;
    getPagingNews(page, size, "").then((value) {
      if (value.isNotEmpty) {
        if (page == 1) {
          value.removeAt(0);
        }
        allNews.addAll(value);
        setState(() {
          loading = false;
        });
      } else {
        setState(() {
          stopLoader = true;
          loading = false;
        });
      }
    }).catchError((err) {
      setState(() {
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final ThemeData themeData = Theme.of(context);
    final sidePadding = EdgeInsets.symmetric(horizontal: PADDING_SIZE);
    double statusBarHeight = MediaQuery.of(context).padding.top;
    final branchService = Provider.of<BranchService>(context);
    final shadarSaidService = Provider.of<ShadarSaidService>(context);

    if (hasInternet) {
      branchService.loadBranchesData().then((value) => null);
      shadarSaidService.loadShadarSaidData().then((value) => null);
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        toolbarHeight: 80,
        title: Image.asset(
          "assets/images/logo_land.png",
          fit: BoxFit.fitWidth,
          height: 38,
        ),
      ),
      endDrawer: hasInternet
          ? Drawer(
              child: Padding(
                  padding: EdgeInsets.only(top: statusBarHeight),
                  child: CustomBranchList()))
          : null,
      body: hasInternet
          ? WillPopScope(
              onWillPop: () async {
                return false;
              },
              child: Container(
                  width: size.width,
                  height: size.height,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: ListView(
                            scrollDirection: Axis.vertical,
                            controller: _scrollController,
                            children: [
                              SizedBox(
                                height: PADDING_SIZE_SMALL,
                              ),
                              Padding(
                                  padding: sidePadding,
                                  child: renderLastNews()),
                              SizedBox(
                                height: PADDING_SIZE_SMALL,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: PADDING_SIZE),
                                child: Divider(
                                  color: COLOR_BLACK,
                                ),
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: PADDING_SIZE),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Сүүлд нэмэгдсэн",
                                            style: themeData
                                                .textTheme.bodyText1!
                                                .merge(TextStyle(
                                                    fontWeight:
                                                        FontWeight.w900))),
                                        TextButton(
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  PageRouteBuilder(
                                                    pageBuilder: (context,
                                                            animation,
                                                            secondaryAnimation) =>
                                                        NewsScreen(),
                                                    transitionDuration:
                                                        Duration(
                                                            milliseconds: 300),
                                                    transitionsBuilder:
                                                        (context,
                                                            animation,
                                                            secondaryAnimation,
                                                            child) {
                                                      return ScaleTransition(
                                                        scale: CurvedAnimation(
                                                            parent: animation,
                                                            curve: Curves
                                                                .fastOutSlowIn),
                                                        child: child,
                                                      );
                                                    },
                                                  ));
                                            },
                                            child: Row(
                                              children: [
                                                Text("Бүгдийг харах",
                                                    style: themeData
                                                        .textTheme.bodyText2!
                                                        .merge(TextStyle(
                                                            color:
                                                                COLOR_BLUE))),
                                                Padding(
                                                  padding:
                                                      EdgeInsets.only(top: 4),
                                                  child: Icon(
                                                    Icons
                                                        .keyboard_arrow_right_rounded,
                                                    color: COLOR_BLUE,
                                                    size: 26,
                                                  ),
                                                ),
                                              ],
                                            ))
                                      ])),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: PADDING_SIZE),
                                  child: CustomNewsList(
                                      key: Key("CustomNewsList"),
                                      newsData: allNews))
                            ],
                          ),
                        )
                      ])),
            )
          : Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset("assets/images/not_internet.png",
                      fit: BoxFit.fitWidth, width: size.width),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: Text("Интернэт холболтоо шалгана уу!",
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: themeData.textTheme.headline1!.merge(TextStyle(
                            color: themeData.textTheme.bodyText2!.color))),
                  )
                ],
              ),
            ),
    );
  }

  // _bottomNavigationBar({IconData icon, String label}) {
  //   return BottomNavigationBarItem(
  //       icon: Icon(
  //         icon,
  //         size: 28,
  //       ),
  //       label: label);
  // }
}
