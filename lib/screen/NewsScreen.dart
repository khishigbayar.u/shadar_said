import 'package:flutter/material.dart';
import 'package:shadar_said/models/BranchModel.dart';
import 'package:shadar_said/models/NewsModel.dart';
import 'package:shadar_said/utils/constants.dart';
import 'package:shadar_said/component/CustomBranchHorizontalList.dart';
import 'package:shadar_said/services/NewsService.dart';
import 'package:shadar_said/screen/DetailScreen.dart';
import 'package:shadar_said/component/CustomNewsDetailFooter.dart';
import 'package:shadar_said/utils/utilFunctions.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class NewsScreen extends StatefulWidget {
  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  String? text;
  String? branchId;
  String filter = "";
  bool stopLoader = false;
  bool loading = false;
  int page = 0;
  int size = 8;
  List<NewsModel> allNews = [];
  final ScrollController _scrollController = ScrollController();

  void initState() {
    super.initState();
    text = "";

    loadNewsData();
    _scrollController.addListener(() {
      var _loading = loading;
      if (_scrollController.position.pixels >=
              _scrollController.position.maxScrollExtent &&
          !stopLoader &&
          !_loading) {
        _loading = true;
        loadNewsData();
      }
    });
  }

  void loadNewsData() {
    setState(() {
      loading = true;
    });
    page = page + 1;
    getPagingNews(page, size, filter).then((value) {
      setState(() {
        loading = false;
      });
      if (value.isNotEmpty) {
        allNews.addAll(value);
      } else {
        setState(() {
          stopLoader = true;
        });
      }
    });
  }

  void loadFilteredNewsData(_filter) {
    setState(() {
      loading = true;
    });

    getPagingNews(1, size, _filter).then((value) {
      setState(() {
        loading = false;
        page = 1;
      });
      if (value.isNotEmpty) {
        setState(() {
          allNews = value;
        });
      } else {
        setState(() {
          allNews = [];
        });
      }
    });
  }

  void changeSearchText(String value) {
    String _filter = "";
    if (!checkIsNullOrEmptyString(branchId) &&
        !checkIsNullOrEmptyString(value)) {
      _filter = "&title=$value&branchId=$branchId";
    } else if (!checkIsNullOrEmptyString(branchId)) {
      _filter = "&branchId=$branchId";
    } else if (!checkIsNullOrEmptyString(value)) {
      _filter = "&title=$value";
    }
    loadFilteredNewsData(_filter);

    setState(() {
      text = value;
      filter = _filter;
    });
  }

  void changeBranch(BranchModel branch) {
    String _filter = "";
    if (!checkIsNullOrEmptyString(branch.id) &&
        !checkIsNullOrEmptyString(text)) {
      _filter = "&title=$text&branchId=${branch.id}";
    } else if (!checkIsNullOrEmptyString(branch.id)) {
      _filter = "&branchId=${branch.id}";
    } else if (!checkIsNullOrEmptyString(text)) {
      _filter = "&title=$text";
    }
    loadFilteredNewsData(_filter);
    setState(() {
      branchId = branch.id;
      filter = _filter;
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final ThemeData themeData = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        toolbarHeight: 80,
        title: TextField(
          onChanged: (text) {
            changeSearchText(text);
          },
          style: themeData.textTheme.bodyText1,
          decoration: InputDecoration(
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(PADDING_SIZE))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(PADDING_SIZE))),
            contentPadding: const EdgeInsets.symmetric(vertical: 12.0),
            fillColor: Colors.grey[200],
            filled: true,
            prefixIcon: Icon(
              Icons.search,
              color: COLOR_GREY,
              size: 24,
            ),
            hintStyle: themeData.textTheme.bodyText2!.merge(TextStyle()),
            hintText: 'Хайх',
          ),
        ),
      ),
      body: Container(
          width: size.width,
          height: size.height,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            CustomBranchHorizontalList(changeBranch: changeBranch),
            allNews.isNotEmpty
                ? Expanded(
                    child: new StaggeredGridView.countBuilder(
                    crossAxisCount: 2,
                    itemCount: allNews.length,
                    itemBuilder: (BuildContext context, int index) =>
                        renderGridNews(allNews[index], context, index),
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.fit(1),
                    mainAxisSpacing: 16,
                    crossAxisSpacing: 16,
                    padding: EdgeInsets.all(16),
                    controller: _scrollController,
                  )
                    // renderGridNews(allNews, context),
                    )
                : (Expanded(
                    child: Center(
                        child: Text(
                    "Мэдээлэл олдсонгүй!",
                    style: themeData.textTheme.headline4!.merge(
                        TextStyle(color: themeData.textTheme.bodyText2!.color)),
                  ))))
          ])),
    );
  }

  renderGridNews(NewsModel newsData, BuildContext context, int index) {
    final ThemeData themeData = Theme.of(context);
    final Size themsize = MediaQuery.of(context).size;
    double itemWidth = (themsize.width - PADDING_SIZE * 3) / 2;

    return GestureDetector(
      key: Key('newsData_$index'),
      onTap: () {
        Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  DetailScreen(selectedNews: newsData),
              transitionDuration: Duration(milliseconds: 300),
              transitionsBuilder:
                  (context, animation, secondaryAnimation, child) {
                var begin = Offset(0.8, 0.0);
                var end = Offset(0, 0);
                var curve = Curves.ease;

                var tween = Tween(begin: begin, end: end)
                    .chain(CurveTween(curve: curve));

                return SlideTransition(
                  position: animation.drive(tween),
                  child: child,
                );
              },
            ));
      },
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.network(
              newsData.imageUrl,
              fit: BoxFit.cover,
              height: itemWidth,
              width: itemWidth,
            ),
          ),
          SizedBox(
            height: PADDING_SIZE_SMALL,
          ),
          Text(
            newsData.title,
            style: themeData.textTheme.headline5,
            maxLines: 3,
          ),
          SizedBox(
            height: PADDING_SIZE_SMALL,
          ),
          CustomNewsDetailFooter(
              category: newsData.category.name,
              createdDate: newsData.createdDate)
        ],
      ),
    );
  }
}
