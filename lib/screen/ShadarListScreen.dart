import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadar_said/models/ShadarSaid.dart';
import 'package:shadar_said/screen/ShadarScreen.dart';
import 'package:shadar_said/services/ShadarSaidService.dart';
import 'package:shadar_said/utils/constants.dart';

class ShadarListScreen extends StatefulWidget {
  @override
  _ShadarListScreenState createState() => _ShadarListScreenState();
}

class _ShadarListScreenState extends State<ShadarListScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final ThemeData themeData = Theme.of(context);
    final shadarSaidService = Provider.of<ShadarSaidService>(context);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          toolbarHeight: 80,
          title: Text(
            "Шадар сайдуудын мэдээлэл",
            style: themeData.textTheme.headline3,
          ),
        ),
        body: Container(
          width: size.width,
          height: size.height,
          child: Column(children: [
            Expanded(
              flex: 1,
              child: ListView.builder(
                  itemCount: shadarSaidService.shadarSaidData.length,
                  padding: EdgeInsets.all(PADDING_SIZE),
                  itemBuilder: (context, index) {
                    return ShadarSaidListItem(
                        themeData: themeData,
                        shadarData: shadarSaidService.shadarSaidData[index]);
                  }
                  // controller: _scrollController,
                  ),
            ),
          ]),
        ));
  }
}

class ShadarSaidListItem extends StatelessWidget {
  const ShadarSaidListItem({
    Key? key,
    required this.themeData,
    required this.shadarData,
  }) : super(key: key);

  final ThemeData themeData;
  final ShadarSaid shadarData;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    ShadarScreen(saidData: shadarData),
                transitionDuration: Duration(milliseconds: 300),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return ScaleTransition(
                    scale: CurvedAnimation(
                        parent: animation, curve: Curves.fastOutSlowIn),
                    child: child,
                  );
                },
              ));
        },
        borderRadius: BorderRadius.circular(BORDER_RADIUS),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: 80,
              height: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(BORDER_RADIUS),
                image: DecorationImage(
                  image: AssetImage(shadarData.imageUrl),
                  fit: BoxFit.cover,
                ),
              ),
              child: null,
            ),
            SizedBox(
              width: PADDING_SIZE_SMALL,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(shadarData.year, style: themeData.textTheme.bodyText1),
                  Text(
                    shadarData.fullName,
                    style: themeData.textTheme.headline2,
                  ),
                  Text(shadarData.position,
                      style: themeData.textTheme.bodyText1),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
