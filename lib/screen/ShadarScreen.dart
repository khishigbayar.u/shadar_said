import 'package:flutter/material.dart';
import 'package:shadar_said/models/ShadarSaid.dart';
import 'package:shadar_said/utils/constants.dart';

class ShadarScreen extends StatefulWidget {
  final ShadarSaid? saidData;

  const ShadarScreen({Key? key, this.saidData}) : super(key: key);

  @override
  _ShadarScreenState createState() =>
      _ShadarScreenState(saidData: this.saidData);
}

class _ShadarScreenState extends State<ShadarScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int selectedIndex = 0;

  final ShadarSaid? saidData;

  _ShadarScreenState({this.saidData});

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  _handleTabSelection(int index) {
    if (_tabController.indexIsChanging) {
      setState(() {
        selectedIndex = index;
        _tabController.animateTo(index);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final ThemeData themeData = Theme.of(context);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          toolbarHeight: 80,
          title: Text(
            "Шадар сайд",
            style: themeData.textTheme.headline3,
          ),
        ),
        body: Container(
          width: size.width,
          height: size.height,
          child: ListView(children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: PADDING_SIZE),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: PADDING_SIZE_SMALL,
                  ),
                  Container(
                    width: size.width,
                    height: 250,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(BORDER_RADIUS),
                      image: DecorationImage(
                        image: AssetImage(saidData!.imageUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: null,
                  ),
                  SizedBox(
                    height: PADDING_SIZE_SMALL,
                  ),
                  Text(
                    saidData!.fullName,
                    style: themeData.textTheme.headline2,
                  ),
                  SizedBox(
                    height: PADDING_SIZE_SMALL,
                  ),
                  Text(
                    saidData!.description,
                    style: themeData.textTheme.bodyText1,
                  ),
                  SizedBox(
                    height: PADDING_SIZE_SMALL,
                  ),
                  TabBar(
                    unselectedLabelColor: Colors.black,
                    labelColor: COLOR_BLUE,
                    indicatorColor: COLOR_BLUE,
                    tabs: [
                      Tab(
                        text: 'Боловсрол',
                      ),
                      Tab(
                        text: 'Ажил',
                      ),
                      Tab(
                        text: 'Шагнал',
                      )
                    ],
                    controller: _tabController,
                    indicatorSize: TabBarIndicatorSize.tab,
                    onTap: _handleTabSelection,
                  ),
                  IndexedStack(
                    children: [
                      Column(
                        children: renderSaidEducations(
                            saidData: saidData, themeData: themeData),
                      ),
                      Column(
                        children: renderSaidWorks(
                            saidData: saidData, themeData: themeData),
                      ),
                      Column(
                        children: renderSaidAwards(
                            saidData: saidData, themeData: themeData),
                      )
                    ],
                    index: selectedIndex,
                  )
                ],
              ),
            ),
          ]),
        ));
  }

  List<Widget> renderSaidEducations(
      {ShadarSaid? saidData, required ThemeData themeData}) {
    List<Widget> results = [];
    for (int i = 0; i < saidData!.educations.length; i++) {
      results.add(Row(
        children: [
          Container(
            alignment: Alignment.center,
            width: 80,
            child: Column(
              children: [
                Text(
                  saidData.educations[i].startYear,
                  style: themeData.textTheme.headline5,
                ),
                Text(
                  "|",
                  style: themeData.textTheme.headline6!.merge(TextStyle(
                    fontSize: 8,
                  )),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  saidData.educations[i].endYear,
                  style: themeData.textTheme.headline5,
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 16,
                ),
                Text(
                  saidData.educations[i].location,
                  style: themeData.textTheme.headline5!.merge(
                      TextStyle(color: themeData.textTheme.bodyText2!.color)),
                ),
                Text(
                  saidData.educations[i].schoolName,
                  style: themeData.textTheme.headline4,
                ),
                Text(
                  saidData.educations[i].degree,
                  style: themeData.textTheme.headline5!.merge(
                      TextStyle(color: themeData.textTheme.bodyText2!.color)),
                ),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
          )
        ],
      ));
    }
    return results;
  }

  List<Widget> renderSaidWorks(
      {ShadarSaid? saidData, required ThemeData themeData}) {
    List<Widget> results = [];
    for (int i = 0; i < saidData!.works.length; i++) {
      results.add(Row(children: [
        Container(
          alignment: Alignment.center,
          width: 80,
          child: Column(
            children: [
              Text(
                saidData.works[i].startYear,
                style: themeData.textTheme.headline5,
              ),
              Text(
                "|",
                style: themeData.textTheme.headline6!.merge(TextStyle(
                  fontSize: 8,
                )),
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                saidData.works[i].endYear,
                style: themeData.textTheme.headline5,
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 16,
              ),
              Text(
                saidData.works[i].position,
                style: themeData.textTheme.headline5!.merge(
                    TextStyle(color: themeData.textTheme.bodyText2!.color)),
              ),
              Text(
                saidData.works[i].officeName,
                style: themeData.textTheme.headline4,
                maxLines: 2,
              ),
              SizedBox(
                height: 16,
              ),
            ],
          ),
        )
      ]));
    }
    return results;
  }

  List<Widget> renderSaidAwards(
      {ShadarSaid? saidData, required ThemeData themeData}) {
    List<Widget> results = [];
    for (int i = 0; i < saidData!.awards!.length; i++) {
      results.add(Row(
        children: [
          Container(
            alignment: Alignment.center,
            width: 80,
            child: Column(
              children: [
                Text(
                  saidData.awards![i].year,
                  style: themeData.textTheme.headline5,
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 16,
              ),
              Text(
                saidData.awards![i].awardName,
                style: themeData.textTheme.headline4,
              ),
              SizedBox(
                height: 16,
              ),
            ],
          )
        ],
      ));
    }
    return results;
  }
}
