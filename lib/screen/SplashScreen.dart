import 'package:flutter/material.dart';
import 'package:shadar_said/screen/HomeScreen.dart';
import 'package:shadar_said/utils/constants.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _navigateToHome();
  }

  _navigateToHome() async {
    await Future.delayed(Duration(milliseconds: 3000), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color(0xFF1265f7),
              Color(0xFF0035a1),
            ],
          )),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 80,
                  height: 100,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/Soyombo.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: null,
                ),
                Padding(
                    padding: EdgeInsets.symmetric(vertical: PADDING_SIZE_SMALL),
                    child: Text("Шадар сайд",
                        style: themeData.textTheme.headline3!
                            .merge(TextStyle(color: Colors.white))))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
