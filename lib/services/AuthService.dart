import 'package:shadar_said/models/ResponseModel.dart';
import 'package:dio/dio.dart';

String baseUrl = 'https://shadar-said.tugscom.mn/api';

Future<ResponseModel?> login() async {
  var dio = Dio();
  try {
    var response = await dio.post('$baseUrl/auth/login',
        data: {'username': 'ShadarAppUser', 'password': '9SGhB?sE'});
    if (response.statusCode != 200) {
      ResponseModel res = new ResponseModel.fromJson(response.data);
      return res;
    }
  } catch (e) {
    throw Exception(
        'Мэдээлэл уншихад алдаа гарлаа. Алдааны код: ${e.toString()}');
  }
}
