import 'package:dio/dio.dart';
import 'package:shadar_said/models/BranchModel.dart';
import 'package:shadar_said/models/ResponseModel.dart';
import 'package:shadar_said/services/AuthService.dart';
import 'package:shadar_said/utils/utilFunctions.dart';

class BranchService {
  String baseUrl = 'https://shadar-said.tugscom.mn/api';
  List<BranchModel> branchData = [];

  BranchService();

  Future loadBranchesData() async {
    var dio = Dio();
    ResponseModel? token = await login();
    dio.options.headers["authorization"] =
        "Bearer ${getNullableString(token!.data)}";

    var response = await dio.get('$baseUrl/branch/getBranches');
    if (response.statusCode != 200) {
      branchData = [];
      for (int i = 0; i < response.data['data'].length; i++) {
        branchData.add(new BranchModel.fromJson(response.data['data'][i]));
      }
    }
  }
}
