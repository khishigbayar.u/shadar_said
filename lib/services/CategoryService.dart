import 'package:dio/dio.dart';
import 'package:shadar_said/models/CategoryModel.dart';
import 'package:shadar_said/models/ResponseModel.dart';
import 'package:shadar_said/services/AuthService.dart';
import 'package:shadar_said/utils/utilFunctions.dart';

String baseUrl = 'https://shadar-said.tugscom.mn/api';

Future<List<CategoryModel>> getCategories() async {
  var dio = Dio();
  ResponseModel? token = await login();
  dio.options.headers["authorization"] = "Bearer ${getNullableString(token!.data)}";
  try {
    var response = await dio.get('$baseUrl/category/getCategories');
    if (response.statusCode != 200) {
      List<CategoryModel> models = [];
      for (int i = 0; i < response.data['data'].length; i++) {
        models.add(new CategoryModel.fromJson(response.data['data'][i]));
      }
      return models;
    }
    throw Exception(
        'Алдааны код: ${response.statusCode}, Алдааны мессеж: ${response.statusMessage}');
  } catch (e) {
    throw Exception(
        'Мэдээлэл уншихад алдаа гарлаа. Алдааны код: ${e.toString()}');
  }
}
