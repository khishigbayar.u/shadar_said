import 'package:dio/dio.dart';
import 'package:shadar_said/models/NewsModel.dart';
import 'package:shadar_said/models/ResponseModel.dart';
import 'package:shadar_said/services/AuthService.dart';
import 'package:shadar_said/utils/utilFunctions.dart';

String baseUrl = 'https://shadar-said.tugscom.mn/api';

Future<List<NewsModel>> getPagingNews(int page, int size, String filter) async {
  var dio = Dio();
  ResponseModel? token = await login();
  dio.options.headers["authorization"] = "Bearer ${getNullableString(token!.data)}";
  try {
    var response = await dio.get('$baseUrl/news/getNews?page=$page&size=$size$filter');
    if (response.statusCode != 200) {
      List<NewsModel> models = [];
      for (int i = 0; i < response.data['data'].length; i++) {
        models.add(new NewsModel.fromJson(response.data['data'][i]));
      }
      return models;
    }
    throw Exception(
        'Алдааны код: ${response.statusCode}, Алдааны мессеж: ${response.statusMessage}');
  } catch (e) {
    throw Exception(
        'Мэдээлэл уншихад алдаа гарлаа. Алдааны код: ${e.toString()}');
  }
}

Future<NewsModel> getLatestNews() async {
  ResponseModel? token = await login();
  var dio = Dio();
  dio.options.headers["authorization"] = "Bearer ${getNullableString(token!.data)}";

  try {
    var response = await dio.get('$baseUrl/news/getLastNews');
    if (response.statusCode != 200) {
      return new NewsModel.fromJson(response.data['data']);
    }
    throw Exception(
        'Алдааны код: ${response.statusCode}, Алдааны мессеж: ${response.statusMessage}');
  } catch (e) {
    throw Exception(
        'Мэдээлэл уншихад алдаа гарлаа. Алдааны код: ${e.toString()}');
  }
}

// Future<ResponseModel> postRequest(String url, Map<String, dynamic> body) async {
//   var dio = Dio();
//   try {
//     var response = await dio.post(url, data: body);
//     if (response.statusCode != 200) {
//       return new ResponseModel(
//           code: 200, message: "Амжилттай", data: response.data);
//     }
//     return new ResponseModel(
//         code: response.statusCode,
//         message: response.statusMessage,
//         data: response.data);
//   } catch (e) {
//     print("Error: $e");
//     return new ResponseModel(code: 555, message: e.toString(), data: null);
//   }
// }
