import 'package:dio/dio.dart';
import 'package:shadar_said/models/ShadarSaid.dart';

class ShadarSaidService {
  String baseUrl = 'https://shadar-said.tugscom.mn/api';
  List<ShadarSaid> shadarSaidData = [];

  ShadarSaidService();

  Future loadShadarSaidData() async {
    var dio = Dio();
    var response = await dio.get('$baseUrl/shadar/saiduud');
    if (response.statusCode != 200) {
      shadarSaidData = [];
      for (int i = 0; i < response.data['data'].length; i++) {
        shadarSaidData.add(new ShadarSaid.fromJson(response.data['data'][i]));
      }
    }
  }
}
