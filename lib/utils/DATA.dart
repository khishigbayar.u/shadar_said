const SPECIAL_NEWS_DATA = [
  {
    'imageUrl': 'assets/images/image1.jpg',
    'title':
        'Шадар сайд С.Амарсайхан Дундговь аймгийн онцгой комиссын гишүүдтэй  цахимаар хуралдлаа',
    'description':
        '04 дүгээр сарын 25-ны өдөр covid-19 цар тахлын бүх нийтийн бэлэн байдлын үед авч буй арга хэмжэ..',
    'category': 'Сонгууль',
    'branch': 'ШӨХТГазар',
    'createdDate': '2021-05-09 01:31:17',
  },
  {
    'imageUrl': 'assets/images/image2.jpg',
    'title':
        'Тээврийн хэрэгслүүд тэгш, сондгойгоор 06:00-22:00 цаг хүртэл зорчино',
    'description':
        'Улсын хэмжээнд бүх нийтийн бэлэн байдлын зэргийг бууруулж,  маргаашаас /2021.05.08/  эхлэн өндө..',
    'category': 'Covid-19',
    'branch': 'ЭМЯ',
    'createdDate': '2021-05-08 18:10:18',
  },
  {
    'imageUrl': 'assets/images/image3.jpg',
    'title': 'Монгол, Хятадаас үүдэлтэй шуурга Өмнөд Солонгосыг дахин нөмөрчээ',
    'description':
        'Хятадын хойд нутаг, Монголын говь цөлөөс үүдэлтэй шороон шуурга Өмнөд Солонгосын баруун нутгийг..',
    'category': 'Дэлхий',
    'branch': 'ШӨХТГазар',
    'createdDate': '2021-05-08 15:10:30',
  }
];
