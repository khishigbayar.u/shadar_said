import 'package:flutter/services.dart';
import 'dart:convert';

import 'package:shadar_said/models/NewsModel.dart';

calculateTime(String createdDate) {
  var date = DateTime.parse(createdDate);
  var day = DateTime.now().difference(date).inDays;
  if (day == 0) {
    var hour = DateTime.now().difference(date).inHours;
    if (hour == 0) {
      var minute = DateTime.now().difference(date).inMinutes;
      if (minute == 0) {
        var seconds = DateTime.now().difference(date).inSeconds;
        return "${seconds} секундын өмнө";
      }
      return "${minute} минутын өмнө";
    }
    return "${hour} цагийн өмнө";
  }

  if (day < 30) {
    return "${day} өдрийн өмнө";
  }

  var month = day ~/ 30;
  if (month < 12) {
    return "${month} сарын өмнө";
  }

  var year = (day ~/ 365).floor();
  return "${year} жилийн өмнө";
}

Future loadJsonData() async {
  return await rootBundle.loadString('lib/utils/test.json');
}

Future<List<NewsModel>> loadNews() async {
  String jsonString = await loadJsonData();
  final response = json.decode(jsonString);
  List<NewsModel> newsData = [];
  for (int i = 0; i < response.length; i++) {
    newsData.add(new NewsModel.fromJson(response[i]));
  }
  return newsData;
}

checkIsNullOrEmptyString(String? text) {
  String _text = getNullableString(text);
  if (_text == '') {
    return true;
  } else {
    return false;
  }
}

getNullableString(String? text) {
  if (text == '' || text == null) {
    return "";
  } else {
    return text;
  }
}
